package controller;

import java.util.ArrayList;


import model.data_structures.NumbersBag;
import model.logic.IntegersBagOperations;

public class Controller<T extends Number>  {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public NumbersBag createBag(ArrayList<T> values){
         return new NumbersBag(values);		
	}
	
	
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static double getMin(NumbersBag bag)
	{
		return model.getMin(bag);
	}
	
	public static double getSum(NumbersBag bag)
	{
		return model.Sum(bag);
	}
	
	public static double addValueToFirst(NumbersBag bag, int x)
	{
		return model.AddValueToFirst(bag , x);
	}
	
	
	
}
