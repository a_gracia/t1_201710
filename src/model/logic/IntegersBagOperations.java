package model.logic;

import java.util.Iterator;


import model.data_structures.NumbersBag;

public class IntegersBagOperations<T extends Number> {

	
	
	public double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext())
			{
				mean += iter.next().doubleValue();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public Double getMax(NumbersBag bag){
		Double max = Double.MIN_NORMAL;
	    T value;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( value.doubleValue() > max){
					max = value.doubleValue();
				}
			}
			
		}
		return max;
	}
	
	public Double getMin(NumbersBag bag)
	{
		double min = Double.MAX_VALUE;
		T value;
		if(bag != null)
		{
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( value.doubleValue() < min)
				{
					min = value.doubleValue();
				}
			}
		}
		return min;
	}
	
	/**
	 * 
	 */
	public double Sum( NumbersBag bag )
	{
		double value = 0;
		Iterator<T> iter = bag.getIterator();
		
		while(iter.hasNext())
		{
			value += iter.next().doubleValue();
		}
		return value;
	}
	
	/**
	 * 
	 */
	public double AddValueToFirst(NumbersBag bag, int x)
	{
		double resp = 0;
		Iterator<T> iter = bag.getIterator();
		if(iter.hasNext())
		{
			double value = iter.next().doubleValue();
			resp = value + x;
		}
		return resp;
	}
	
	
}
